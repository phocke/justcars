# Bootstrap instructions

### To setup the justcars API application:

```
cd justcars-api
bundle install
rake db:setup
```

### To setup the justcars frontend application:

```
cd justcars-frontend
npm install
```

### To start both applications with a single command (it is configured in Procfile):

```
gem install foreman
foreman start
```

### Alternatively if you don't want to use foreman you can start both apps separately:

##### API

```
cd justcars-api
rails server
```

##### Frontend

```
cd justcars-frontend
npm install
npm start
```

Navigate to `http://localhost:3001/` to see your app
